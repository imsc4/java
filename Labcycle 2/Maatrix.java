package s2.maatrix;
import java.util.Scanner;
public class Maatrix {
    private int rows;
    private int coloumns;
    private double data[][];
    private String name;
    public Maatrix(int r , int c){
        this.rows=r;
        this.coloumns=c;
        this.data=new double[rows][coloumns];
        
    }
    public void SetElement(int row ,int coloumn ,double value){
        if(row>=0 && row<rows && coloumn>=0 && coloumn<coloumns){
            data[row][coloumn]=value;
            
        }
        else{
            System.out.println("Invalid Position");
        }
    }
    
    public double getElement(int row, int coloumn){
        if(row>=0 && row<rows && coloumn>=0 && coloumn<coloumns){
            return data[row][coloumn];
            
        }
        else{
            System.out.println("invalid position");
            return -1;
            
        }
    }
    public void display(){
        System.out.println("Matrix : ");
        for(int i=0;i<rows;i++){
            for(int j=0;j<coloumns;j++){
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }
    public Maatrix add (Maatrix mat1){
        if(this.rows!=mat1.rows || this.coloumns!=mat1.coloumns){
            System.out.println("This Matrices cannot be added");
            return null;
        }
        else{
            Maatrix sum = new Maatrix (this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=this.getElement(i,j)+mat1.getElement(i,j);
                    sum.SetElement(i,j, value);
                    
                }
            }
             return sum;   
            }
        }
    public Maatrix sub (Maatrix mat2){
        if(this.rows!=mat2.rows || this.coloumns!=mat2.coloumns){
            System.out.println("This matrices cannot be substracted");
            return null;
        }
        else{
            Maatrix diff= new Maatrix(this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=this.getElement(i,j)-mat2.getElement(i,j);
                    diff.SetElement(i,j, value);
                }
            }
            return diff;
        }
    }
    public Maatrix mul (Maatrix mat3){
        if(this.coloumns!=mat3.rows){
            System.out.println("This matrices cannot be multiplied");
            return null;
        }
        else{
            Maatrix prod=new Maatrix (this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=0;
                    for(int k=0;k<this.coloumns;k++){
                        value+=this.getElement(i,k)*mat3.getElement(k,j);
                    }
                    prod.SetElement(i,j, value);
                }
            }
            return prod;
        }
    }
    public Maatrix transpose(){
        Maatrix transpose=new Maatrix(this.coloumns,this.rows);
        for(int i=0;i<this.rows;i++){
            for(int j=0;j<this.coloumns;j++){
                transpose.SetElement(j,i,this.getElement(i,j));
            }
        }
        return transpose;
    }
    public String toString(){
        StringBuilder string= new StringBuilder();
        for(int i=0;i<rows;i++){
            for(int j=0;j<coloumns;j++){
                string.append(getElement(i,j)).append(" ");
            }
        }
        return string.toString();
    }
    
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        System.out.println("First matrix :");
        System.out.println("Number of rows of the matrix = ");
        int row=scanner.nextInt();
        System.out.println("Enter the number of coloumns of the matrix = ");
        int cols=scanner.nextInt();
        
        Maatrix mat1=new Maatrix(row,cols);
        System.out.println("Enter the elements of the matrix : ");
        for(int i=0;i<row;i++){
            for(int j=0;j<cols;j++){
                int val=scanner.nextInt();
                mat1.SetElement(i,j,val);
            }
        }
        mat1.display();
        
        System.out.println();
        System.out.println("Second matrix : ");
        System.out.println("Number of rows of matrix : ");
        int r=scanner.nextInt();
        System.out.println("Number of coloumns of matrix : ");
        int c=scanner.nextInt();
        
        Maatrix mat2=new Maatrix(r,c);
        System.out.println("Enter the elements of matrix : ");
        for(int i=0;i<r;i++){
            for(int j=0;j<r;j++){
                int val=scanner.nextInt();
                mat2.SetElement(i,j, val);
            }
        }
        mat2.display();
        System.out.println();
        
        System.out.println("Addition of two matrices : ");
        Maatrix mat3=mat1.add(mat2);
        if(mat3!=null){
            mat3.display();
        }
        System.out.println();
        
        System.out.println("Difference of two matrices ");
        Maatrix mat4=mat1.sub(mat2);
        if(mat4!=null){
            mat4.display();
        }
        System.out.println(); 
        
         System.out.println("Multiplication of two matrices : ");
         Maatrix mat5=mat1.mul(mat2);
         if(mat5!=null){
             mat5.display();
         }
         System.out.println();
         
         System.out.println("Transpose of first matrix is : ");
         Maatrix mat6=mat1.transpose();
         mat6.display();
         System.out.println();
         System.out.println();
         
         System.out.println(mat1.toString());
         
         System.out.println("Enter the order of square matrix :");
         int order=scanner.nextInt();
         SquareMatrix squareMatrix;
        squareMatrix = new SquareMatrix (order);
         
         System.out.println("Enter the Elements for the Square matrix :");
         for(int i=0;i<order;i++){
             for(int j=0;j<order;j++){
                 squareMatrix.SetElement(i,j,scanner.nextDouble());
             }
         }
         System.out.println("\n Square Matrix :");
         System.out.println(squareMatrix);
        
         System.out.println("Determinant of the square matrix = ");
         System.out.println(squareMatrix.determinant());
         
         System.out.println("Is the Matrix Symmetric : ");
         System.out.println(squareMatrix.isSymmetric());
         
         System.out.println("Is the Matrix Singular : ");
         System.out.println(squareMatrix.isSingular());
         
         System.out.println("Trace of the Square Matrix :");
         System.out.println(squareMatrix.trace());
         System.out.println();
         
        System.out.println("Enter the order for the diagonal Matrix :");
        int diaOrder = scanner.nextInt();
        double[] diaValues= new double [diaOrder];
        
        System.out.println("Enter the Diagonal Elements : ");
        for(int i=0;i<diaOrder;i++){
            diaValues[i]=scanner.nextDouble();
        }
        
        DiagonalMatrix diaMatrix= new DiagonalMatrix (diaOrder,diaValues);
        System.out.println("Diagonal Matrix :\n");
        diaMatrix.display();
    }
}   
     class SquareMatrix extends Maatrix{
        public int order;
        
        public SquareMatrix(int order){
            super(order,order);
            this.order=order;
        }
        
        
        public double determinant(){
            if(order == 1){
                return this.getElement(0,0);
            }
            else if(this.order==2){
                  return (this.getElement(0, 0) * this.getElement(1, 1)) - (this.getElement(0, 1) * this.getElement(1, 0));
        } else {
            double det = 0;
            for (int i = 0; i < this.order; i++) {
                det+= Math.pow(-1, i) * this.getElement(0, i) * this.minor(0, i).determinant();
            }
            return det;
            }
        }
        
        
    private SquareMatrix minor(int row,int col){
           SquareMatrix minor= new SquareMatrix (this.order-1);
           int m=0;
           for(int i=0;i<this.order;i++){
               if(i==row)continue;
               int n=0;
               for(int j=0;j<this.order;j++){
                   if(j==col)continue;
                   minor.SetElement(m,n,this.getElement(i,j));
                   n++;
               }
               m++;
       }
           return minor;
    }
       
       
    public boolean isSymmetric(){
        return this.equals(this.transpose());
    }
       
    public boolean isSingular() {
        return this.determinant() == 0;
    }

    public double trace() {
        double trace = 0;
        for (int i = 0; i < this.order; i++) {
            trace += this.getElement(i, i);
        }
        return trace;
    }
}

class DiagonalMatrix extends SquareMatrix {
    public DiagonalMatrix(int size, double values[]) {
        super(size);
        for (int i = 0; i < size; i++) {
            this.SetElement(i, i, values[i]);
        }
    }
}
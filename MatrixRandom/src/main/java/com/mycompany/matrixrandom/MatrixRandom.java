import java.util.Random;
public class MatrixRandom {
    private int[][] matrix;
    private int rows; // Added instance variable for rows
    private int cols; // Added instance variable for columns

    public MatrixRandom() {
        Random random = new Random();
        rows = random.nextInt(11); // Initialize rows
        cols = random.nextInt(11); // Initialize columns
        matrix = new int[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = random.nextInt(1000) + 1;
            }
        }
    }

    public void displayMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        MatrixRandom randomMatrix = new MatrixRandom();
        System.out.println("Random Matrix:");
        System.out.println("Rows: " + randomMatrix.rows); // Display rows
        System.out.println("Columns: " + randomMatrix.cols); // Display columns
        randomMatrix.displayMatrix();
    }
}

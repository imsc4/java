/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package s2.datafield;
import java.util.Date;
public class DataField {
    private static int ObjectCount=0;
    private Date date;
    private int UniqueID;
    
    public DataField(){
        ObjectCount++;
        date= new Date();
        UniqueID=ObjectCount;
    }
    
    public void displayData(){
        System.out.println("Date : "+date);
        System.out.println("Unique Id : "+UniqueID);
    }
    
    public static void main(String[] args){
        DataField obj= new DataField();
        obj.displayData();
        
        DataField obj1= new DataField();
        obj1.displayData();
    }
}

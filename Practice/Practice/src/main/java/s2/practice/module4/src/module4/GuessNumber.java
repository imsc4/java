/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package module4;

/**
 *
 * @author muhsi
 */
import java.util.Scanner;
public class GuessNumber {
    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        int num = (int)(Math.random()*101);
        
        int guess= -1;
        while(guess != num){
            System.out.println("Enter your Guess : ");
            guess=in.nextInt();
        
            if(guess == num)
                System.out.println("You guessed Correctly !!");
            else if(guess > num )
                System.out.println("Your guess is too high");
            else
                System.out.println("Your Guess is too low");
            }
        
    }
}

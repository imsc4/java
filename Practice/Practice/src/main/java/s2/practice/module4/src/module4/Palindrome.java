/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package module4;

/**
 *
 * @author muhsi
 */
import java.util.Scanner;
public class Palindrome {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a String : ");
        String s = in.nextLine();
        int low=0;
        int high =s.length() - 1;
        boolean isPal = true;
        while(low < high){
            if(s.charAt(low) != s.charAt(high))
                isPal=false;
            System.out.println(s+" is not a palindrome");
            break;
        }
        low--;
        high++;
    }
}

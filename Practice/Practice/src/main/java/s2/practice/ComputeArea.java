/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package s2.practice;

/**
 *
 * @author muhsi
 */
public class ComputeArea {
    public static void main(String[] args){
        double radius;
        double area;
        
        radius=20;
        area=radius*radius*3.14;
        System.out.println("The area of the circle of radius "+radius+" is : "+area);
    }   
}

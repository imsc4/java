/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package module5;

/**
 *
 * @author muhsi
 */
public class TestVoid {
    public static void main(String[] args){
        System.out.println("Your Grade is :\n");
        PrintGrade(95.5);
        System.out.println("   \n");
        System.out.print("The grade is " + getGrade(78.5)+" \n");
    }
    
    public static void PrintGrade(double s){
        if(s >= 90.0){
            System.out.print("A");
        }
        else if(s >= 80.0){
        System.out.print("B");
        }
        else if (s >= 70.0){
            System.out.print("C");
        }
        else if(s <= 60.0){
            System.out.print("D");
        }
        else
           System.out.print("E");

    }
    
    public static char getGrade(double score){
        if (score >= 90.0)
      return 'A';
    else if (score >= 80.0)
      return 'B';
    else if (score >= 70.0)
      return 'C';
    else if (score >= 60.0)
      return 'D';
    else
      return 'F';
    }
}

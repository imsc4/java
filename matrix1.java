import java.util.Scanner;

public class matrix1 {
    private int rows;
    private int columns;
    private double data[][];
    private String name;

    public matrix1(int r , int c){
        this.rows=r;
        this.columns=c;
        this.data=new double[rows][columns];
    }

    public void SetElement(int row ,int column ,double value){
        if(row>=0 && row<rows && column>=0 && column<columns){
            data[row][column]=value;
        }
        else{
            System.out.println("Invalid Position");
        }
    }

    public double getElement(int row, int column){
        if(row>=0 && row<rows && column>=0 && column<columns){
            return data[row][column];
        }
        else{
            System.out.println("invalid position");
            return -1;
        }
    }

    public void display(){
        System.out.println("Matrix : ");
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }

    public matrix1 add (matrix1 mat1){
        if(this.rows!=mat1.rows || this.columns!=mat1.columns){
            System.out.println("This Matrices cannot be added");
            return null;
        }
        else{
            matrix1 sum = new matrix1 (this.rows,this.columns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.columns;j++){
                    double value=this.getElement(i,j)+mat1.getElement(i,j);
                    sum.SetElement(i,j, value);
                }
            }
            return sum;
        }
    }

    public matrix1 sub (matrix1 mat2){
        if(this.rows!=mat2.rows || this.columns!=mat2.columns){
            System.out.println("This matrices cannot be substracted");
            return null;
        }
        else{
            matrix1 diff= new matrix1(this.rows,this.columns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.columns;j++){
                    double value=this.getElement(i,j)-mat2.getElement(i,j);
                    diff.SetElement(i,j, value);
                }
            }
            return diff;
        }
    }

    public matrix1 mul (matrix1 mat3){
        if(this.columns!=mat3.rows){
            System.out.println("This matrices cannot be multiplied");
            return null;
        }
        else{
            matrix1 prod=new matrix1 (this.rows,mat3.columns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<mat3.columns;j++){
                    double value=0;
                    for(int k=0;k<this.columns;k++){
                        value+=this.getElement(i,k)*mat3.getElement(k,j);
                    }
                    prod.SetElement(i,j, value);
                }
            }
            return prod;
        }
    }

    public matrix1 transpose(){
        matrix1 transpose=new matrix1(this.columns,this.rows);
        for(int i=0;i<this.rows;i++){
            for(int j=0;j<this.columns;j++){
                transpose.SetElement(j,i,this.getElement(i,j));
            }
        }
        return transpose;
    }

    public String toString(){
        StringBuilder string= new StringBuilder();
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                string.append(getElement(i,j)).append(" ");
            }
        }
        return string.toString();
    }

    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        System.out.println("First matrix :");
        System.out.println("Number of rows of the matrix = ");
        int row=scanner.nextInt();
        System.out.println("Enter the number of columns of the matrix = ");
        int cols=scanner.nextInt();
        matrix1 mat1=new matrix1(row,cols);
        System.out.println("Enter the elements of the matrix : ");
        for(int i=0;i<row;i++){
            for(int j=0;j<cols;j++){
                double val=scanner.nextDouble();
                mat1.SetElement(i,j,val);
            }
        }
        mat1.display();
        System.out.println();

        System.out.println("Second matrix : ");
        System.out.println("Number of rows of matrix : ");
        int r=scanner.nextInt();
        System.out.println("Number of columns of matrix : ");
        int c=scanner.nextInt();
        matrix1 mat2=new matrix1(r,c);
        System.out.println("Enter the elements of matrix : ");
        for(int i=0;i<r;i++){
            for(int j=0;j<c;j++){
                double val=scanner.nextDouble();
                mat2.SetElement(i,j, val);
            }
        }
        mat2.display();
        System.out.println();

        System.out.println("Addition of two matrices : ");
        matrix1 mat3=mat1.add(mat2);
        if(mat3!=null){
            mat3.display();
        }
        System.out.println();

        System.out.println("Difference of two matrices ");
        matrix1 mat4=mat1.sub(mat2);
        if(mat4!=null){
            mat4.display();
        }
        System.out.println();

        System.out.println("Multiplication of two matrices : ");
        matrix1 mat5=mat1.mul(mat2);
        if(mat5!=null){
            mat5.display();
        }
        System.out.println();

        System.out.println("Transpose of first matrix is : ");
        matrix1 mat6=mat1.transpose();
        mat6.display();
        System.out.println();
        System.out.println();

        System.out.println(mat1.toString());

        // close the scanner
        scanner.close();
    }
}
